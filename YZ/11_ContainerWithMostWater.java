/*
11. Container With Most Water  QuestionEditorial Solution  My Submissions
Total Accepted: 88511
Total Submissions: 249714
Difficulty: Medium
Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.

Note: You may not slant the container.

Subscribe to see which companies asked this question

Hide Tags Array Two Pointers
Hide Similar Problems (H) Trapping Rain Water

*/


public class Solution {
    public int maxArea(int[] height) {
        int pt1=0, pt2=height.length-1;
        int max=0;
        while(pt1<=pt2){
            max=Math.max(max,Math.min(height[pt1], height[pt2])*(pt2-pt1));
            if(height[pt1]<height[pt2]){
                pt1++;
            }else{
                pt2--;
            }
        }
        return max;
    }
}
