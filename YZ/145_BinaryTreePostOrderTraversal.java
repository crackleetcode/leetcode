/*
145. Binary Tree Postorder Traversal  QuestionEditorial Solution  My Submissions
Total Accepted: 107711
Total Submissions: 293570
Difficulty: Hard
Given a binary tree, return the postorder traversal of its nodes' values.

For example:
Given binary tree {1,#,2,3},
   1
    \
     2
    /
   3
return [3,2,1].

Note: Recursive solution is trivial, could you do it iteratively?

Subscribe to see which companies asked this question

Hide Tags Tree Stack
Hide Similar Problems (M) Binary Tree Inorder Traversal

*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result=new ArrayList<Integer>();
        Stack<TreeNode> stack=new Stack<TreeNode>();
        pushLeft(stack, root);
        TreeNode lastVisited=null;
        while(stack.size()>0){
        	TreeNode node=stack.peek();
        	if(node.right==null || lastVisited==node.right){
        	    stack.pop();
        		result.add(node.val);
        		lastVisited=node;
        	}else{
        		pushLeft(stack, node.right);
        	}
        }
        return result;
    }

    private void pushLeft(Stack<TreeNode> stack, TreeNode node){
    	while(node!=null){
    		stack.push(node);
    		node=node.left;
    	}
    }
}
