/*
162. Find Peak Element  QuestionEditorial Solution  My Submissions
Total Accepted: 75941
Total Submissions: 222828
Difficulty: Medium
A peak element is an element that is greater than its neighbors.

Given an input array where num[i] ≠ num[i+1], find a peak element and return its index.

The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.

You may imagine that num[-1] = num[n] = -∞.

For example, in array [1, 2, 3, 1], 3 is a peak element and your function should return the index number 2.

click to show spoilers.

Note:
Your solution should be in logarithmic complexity.

Credits:
Special thanks to @ts for adding this problem and creating all test cases.

Subscribe to see which companies asked this question

Hide Tags Binary Search Array

*/


public class Solution {
    public int findPeakElement(int[] nums) {
        int left=0, right=nums.length-1;
        while(left<=right){
            int mid=right-(right-left)/2;
            int result=checkPeak(mid, nums);
            if(result==0){
                return mid;
            }else if(result==1){
                left=mid+1;
            }else{
                right=mid-1;
            }
        }
        return left;
    }

    private int checkPeak(int i, int[] nums){
        int left=i-1<0?Integer.MIN_VALUE:nums[i-1];
        int right=i+1>=nums.length?Integer.MIN_VALUE:nums[i+1];
        if(left<nums[i]){
            if(right<nums[i]){
                return 0;
            }else{
                return 1;
            }
        }else{
            // if(right>nums[i]){
            //     //both side is fine, here we choose left for simplicity
            // }else{
            //     // go left
            //     return -1;
            // }
            return -1;
        }
    }
}
