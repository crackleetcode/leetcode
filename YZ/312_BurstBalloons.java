/*
312. Burst Balloons  QuestionEditorial Solution  My Submissions
Total Accepted: 13081
Total Submissions: 33042
Difficulty: Hard
Given n balloons, indexed from 0 to n-1. Each balloon is painted with a number on it represented by array nums. You are asked to burst all the balloons. If the you burst balloon i you will get nums[left] * nums[i] * nums[right] coins. Here left and right are adjacent indices of i. After the burst, the left and right then becomes adjacent.

Find the maximum coins you can collect by bursting the balloons wisely.

Note:
(1) You may imagine nums[-1] = nums[n] = 1. They are not real therefore you can not burst them.
(2) 0 ≤ n ≤ 500, 0 ≤ nums[i] ≤ 100

Example:

Given [3, 1, 5, 8]

Return 167

    nums = [3,1,5,8] --> [3,5,8] -->   [3,8]   -->  [8]  --> []
   coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167
Credits:
Special thanks to @dietpepsi for adding this problem and creating all test cases.

Subscribe to see which companies asked this question

Hide Tags Divide and Conquer Dynamic Programming

*/

public class Solution {
    public int maxCoins(int[] nums) {
        if(nums==null || nums.length==0){
            return 0;
        }
        //left inclusive, right exclusive
        // dp[i][j] = max(dp[i][k]+dp[k+1][j]+nums[k]*nums[i-1]*nums[j])    k in [i, j)
        int[][] dp=new int[nums.length+1][nums.length+1];
        for(int i=0; i<=nums.length; i++){
            dp[i][i]=0;
        }
        for(int len=1; len<=nums.length; len++){
            for(int i=0; i<nums.length-len+1; i++){
                int j=i+len;
                dp[i][j]=0;
                for(int k=i; k<j; k++){
                    int left=i-1<0?1:nums[i-1], right=j>=nums.length?1:nums[j];
                    dp[i][j]=Math.max(dp[i][j], dp[i][k]+dp[k+1][j]+nums[k]*left*right);
                }
            }
        }
        return dp[0][nums.length];
    }
}
