/*
321. Create Maximum Number  QuestionEditorial Solution  My Submissions
Total Accepted: 9304
Total Submissions: 41392
Difficulty: Hard
Given two arrays of length m and n with digits 0-9 representing two numbers. Create the maximum number of length k <= m + n from digits of the two. The relative order of the digits from the same array must be preserved. Return an array of the k digits. You should try to optimize your time and space complexity.

Example 1:
nums1 = [3, 4, 6, 5]
nums2 = [9, 1, 2, 5, 8, 3]
k = 5
return [9, 8, 6, 5, 3]

Example 2:
nums1 = [6, 7]
nums2 = [6, 0, 4]
k = 5
return [6, 7, 6, 0, 4]

Example 3:
nums1 = [3, 9]
nums2 = [8, 9]
k = 3
return [9, 8, 9]

Credits:
Special thanks to @dietpepsi for adding this problem and creating all test cases.

Subscribe to see which companies asked this question

Hide Tags Dynamic Programming Greedy

*/


public class Solution {
    public int[] maxNumber(int[] nums1, int[] nums2, int k) {
        int[] max=new int[0];
        for(int i=0; i<=k; i++){
            int k1=i;
            int k2=k-i;
            if(k1<=nums1.length && k2<=nums2.length){
                int[] localMax1=findMaxK(nums1, k1);
                int[] localMax2=findMaxK(nums2, k2);
                int[] localMax=maxNumberFromTwoArrays(localMax1, localMax2);
                if(isGreater(localMax, max, 0, 0)){
                    max=localMax;
                }
            }
        }
        return max;
    }

    // max number constructed by k numbers from the array, order maintained
    private int[] findMaxK(int[] nums, int k){
        if(k==0){
			return new int[0];
		}
        Deque<Integer> dq=new LinkedList<Integer>();
        int windowSize=nums.length-k+1;
        int[] result=new int[k];
        for(int i=0; i<nums.length; i++){
            while(dq.size()>0 && dq.peekLast()<nums[i]){
                dq.pollLast();
            }
            dq.offerLast(nums[i]);
            if(i>=windowSize-1){
                result[i-windowSize+1]=dq.pollFirst();
            }
        }
        return result;
    }

    private int[] maxNumberFromTwoArrays(int[] nums1, int[] nums2){
        int[] result=new int[nums1.length+nums2.length];
        int pt1=0, pt2=0;
        while(pt1<nums1.length || pt2<nums2.length){
            if(isGreater(nums1, nums2, pt1, pt2)){
                result[pt1+pt2]=nums1[pt1];
                pt1++;
            }else{
                result[pt1+pt2]=nums2[pt2];
                pt2++;
            }
        }
        return result;
    }

    private boolean isGreater(int[] nums1, int[] nums2, int pt1, int pt2){
        while(pt1<nums1.length && pt2<nums2.length){
            if(nums1[pt1]>nums2[pt2]){
                return true;
            }else if(nums1[pt1]<nums2[pt2]){
                return false;
            }else{
                pt1++;
                pt2++;
            }
        }
        return pt2>=nums2.length;
    }
}
