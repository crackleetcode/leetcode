/*
384. Shuffle an Array  QuestionEditorial Solution  My Submissions
Total Accepted: 244
Total Submissions: 555
Difficulty: Medium
Shuffle a set of numbers without duplicates.

Example:

// Init an array with set 1, 2, and 3.
int[] nums = {1,2,3};
Solution solution = new Solution(nums);

// Shuffle the array [1,2,3] and return its result. Any permutation of [1,2,3] must equally likely to be returned.
solution.shuffle();

// Resets the array back to its original configuration [1,2,3].
solution.reset();

// Returns the random shuffling of array [1,2,3].
solution.shuffle();
Subscribe to see which companies asked this question

Have you met this question in a real interview? Yes
*/


/*
##########
Math.random is more biased
use random.nextInt instead of Math.random!
##########
*/

public class Solution {
    int[] org;
    int[] shuf;
    Random rand;
    public Solution(int[] nums) {
        org=new int[nums.length];
        shuf=new int[nums.length];
        rand=new Random();
        for(int i=0; i<nums.length; i++){
            org[i]=nums[i];
            shuf[i]=nums[i];
        }
    }

    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        for(int i=0; i<org.length; i++){
            shuf[i]=org[i];
        }
        return shuf;
    }

    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        for(int i=shuf.length-1; i>=0; i--){
            int targetIndex=rand.nextInt(i+1);
            swap(shuf, targetIndex, i);
        }
        return shuf;
    }

    private void swap(int[] arr, int i, int j){
        int temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
    }
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int[] param_1 = obj.reset();
 * int[] param_2 = obj.shuffle();
 */
