/*
3. Longest Substring Without Repeating Characters  QuestionEditorial Solution  My Submissions
Total Accepted: 172673
Total Submissions: 754927
Difficulty: Medium
Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.

Subscribe to see which companies asked this question

Hide Tags Hash Table Two Pointers String
Hide Similar Problems (H) Longest Substring with At Most Two Distinct Characters

*/

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        if(s==null || s.length()==0){
            return 0;
        }
        int pt=0;
        int maxLen=0;
        Set<Character> set=new HashSet<Character>();
        for(int i=0; i<s.length(); i++){
            char c=s.charAt(i);
            while(set.contains(c)){
                set.remove(s.charAt(pt));
                pt++;
            }
            set.add(c);
            maxLen=Math.max(set.size(), maxLen);
        }
        return maxLen;
    }
}
